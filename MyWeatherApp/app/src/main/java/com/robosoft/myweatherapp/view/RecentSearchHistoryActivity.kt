package com.robosoft.myweatherapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.adapter.RecentSearchViewAdapter
import com.robosoft.myweatherapp.databinding.ActivityRecentSearchHistoryBinding
import com.robosoft.myweatherapp.model.WeatherData
import com.robosoft.myweatherapp.model.database.WeatherDAO
import com.robosoft.myweatherapp.model.database.WeatherDataBase
import com.robosoft.myweatherapp.model.database.WeatherDataRepository
import com.robosoft.myweatherapp.model.database.WeatherViewModelFactory
import com.robosoft.myweatherapp.viewmodel.SearchViewModel

class RecentSearchHistoryActivity : AppCompatActivity() {

    lateinit var binding: ActivityRecentSearchHistoryBinding
    lateinit var recentSearchHistoryViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recent_search_history)
        val dao: WeatherDAO = WeatherDataBase.getInstance(application).weatherDao

        val weatherRepository = WeatherDataRepository(dao)
        val factory = WeatherViewModelFactory(weatherRepository)
        recentSearchHistoryViewModel = ViewModelProvider(this, factory).get(SearchViewModel::class.java)
        binding.searchViewModel = recentSearchHistoryViewModel
        binding.lifecycleOwner = this

        initRecyclerView()

    }
    private fun listItemClicked(weatherData: WeatherData) {

        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra("intentName", "RECENT_SEARCH")
        intent.putExtra("cityName", weatherData.cityName)
        intent.putExtra("intentRecent", weatherData.recentSearch)
        startActivity(intent)

    }
    private fun favBtnClicked(weatherData: WeatherData) {
       recentSearchHistoryViewModel.removeOrAddToFavourites(weatherData)
    }
    private fun initRecyclerView() {
        binding.RecentSearchRecyclerView.layoutManager = LinearLayoutManager(this)
        displayAllSearchData()
    }
    private fun displayAllSearchData() {
        recentSearchHistoryViewModel.recentSearchWeatherData.observe(this, Observer {
            if (it.isEmpty()) {
                startActivity(Intent(this, RecentSearchBlankActivity::class.java))
            }
            binding.RecentSearchRecyclerView.adapter = RecentSearchViewAdapter(it, { selectedItem: WeatherData -> listItemClicked(selectedItem)}, {item:WeatherData -> favBtnClicked(item)}, this)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.fav_search_bar_menu, menu)
        var searchItem = menu!!.findItem(R.id.favouritesMenu)
        if(searchItem != null){
            val searchView = searchItem.actionView as SearchView
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }

            })
        }

        return super.onCreateOptionsMenu(menu)
    }
}
