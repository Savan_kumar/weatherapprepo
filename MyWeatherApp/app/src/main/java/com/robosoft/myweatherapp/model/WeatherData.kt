package com.robosoft.myweatherapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "weather_data")
data class WeatherData(

    @PrimaryKey(autoGenerate = false)
    val locationCityId: Long,

    val cordLat: Double,

    val cordLon: Double,

    val dataTimeValue: String,

    val locationDataValue: String,

    val cityName: String,

    val imageIconId: String,

    val temperatureValue: String,

    val temperatureValueKelvin: Double,

    val temperatureDescriptionValue: String,

    val minMaxValue: String,

    val precipitationValue: String,

    val humidityValue: String,

    val windValue: String,

    val visibilityValue: String,

    val favouriteCity: Boolean,

    val recentSearch:Boolean

)
