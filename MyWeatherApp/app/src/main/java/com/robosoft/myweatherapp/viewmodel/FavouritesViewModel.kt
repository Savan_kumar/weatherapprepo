package com.robosoft.myweatherapp.viewmodel

import android.content.DialogInterface
import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.model.database.WeatherDataRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FavouritesViewModel(val dataRepository: WeatherDataRepository): ViewModel(), Observable {

    val favWeatherData = dataRepository.favWeatherData
    @Bindable
    val noOfCity = MutableLiveData<String>()
    @Bindable
    val onInitialExecution = MutableLiveData<Boolean>()
    init{
        onInitialExecution.value = true
    }
    fun removeFavouriteCity(cityId: Long) {
        removeFavCity(cityId)
    }
    fun removeAllFavCity(view: View) {
        val context = view.context
        val alertDialog = androidx.appcompat.app.AlertDialog.Builder(context, R.style.CustomDialog)
        alertDialog.setMessage("Are you sure you want to remove all the favourites")
        alertDialog.setPositiveButton("YES") {dialog: DialogInterface?, which: Int ->
            removeAllFavourites()
        }
        alertDialog.setNegativeButton("NO") {dialog: DialogInterface?, which: Int ->
            //
        }
        alertDialog.create()
        alertDialog.show()
    }
    private fun removeAllFavourites(): Job = viewModelScope.launch {
        dataRepository.removeAllFav()
    }

    private fun removeFavCity(cityId: Long): Job = viewModelScope.launch {
        dataRepository.removeFav(cityId)
    }














    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }


}