/*
 * *****************************************************************************
 * WeatherApiInterface.kt
 * MyWeatherApp
 *
 * Created by savankumar on 15/9/20 2:56 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.myweatherapp.api

import com.robosoft.myweatherapp.model.ResponseDataClass
import com.robosoft.myweatherapp.model.WeatherData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiInterface {
    
    @GET("weather")
    fun getWeatherData(
        @Query("q") cityName: String,
        @Query("APPID") appID: String
    ):Call<ResponseDataClass>

}