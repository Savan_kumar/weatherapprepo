package com.robosoft.myweatherapp.model.database


import com.robosoft.myweatherapp.model.WeatherData

class WeatherDataRepository(val weatherDao: WeatherDAO) {

    val weatherData = weatherDao.getAllWeatherData()
    val favWeatherData = weatherDao.getAllFavourites(true)
    val recentWeatherData = weatherDao.getAllRecentSearch(true)


    suspend fun insertData(weatherData: WeatherData) {
        weatherDao.insertWeatherData(weatherData)
    }
    suspend fun deleteAllData() {
        weatherDao.deleteAll()
    }

    suspend fun removeFav(cityID: Long) {
        weatherDao.removeCityFromFavourites(cityID, false)
    }
    suspend fun removeAllFav() {
        weatherDao.removeAllFavCity(false, true)
    }

    suspend fun removeAllSearchData() {
        weatherDao.removeAllRecentData(false, true)
    }



    fun getCityDetails(cityID: Long): List<WeatherData> {
        return weatherDao.getCityDetailByID(cityID)
    }

}