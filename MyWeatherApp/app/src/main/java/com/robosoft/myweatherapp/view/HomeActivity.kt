package com.robosoft.myweatherapp.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.databinding.ActivityHomeBinding
import com.robosoft.myweatherapp.model.database.WeatherDAO
import com.robosoft.myweatherapp.model.database.WeatherDataBase
import com.robosoft.myweatherapp.model.database.WeatherDataRepository
import com.robosoft.myweatherapp.model.database.WeatherViewModelFactory
import com.robosoft.myweatherapp.viewmodel.WeatherViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    lateinit var binding: ActivityHomeBinding
    lateinit var weatherViewModel: WeatherViewModel
    var searchValue = ""
    lateinit var toggle: ActionBarDrawerToggle

    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        setUpToolbar()

        val dao: WeatherDAO = WeatherDataBase.getInstance(application).weatherDao

        val weatherRepository = WeatherDataRepository(dao)
        val factory = WeatherViewModelFactory(weatherRepository)
        val homeActivityContext  = this
        weatherViewModel = ViewModelProvider(this, factory).get(WeatherViewModel::class.java)
        binding.weatherViewModel = weatherViewModel
        binding.lifecycleOwner = this
        var intentHome = intent
        var intentCityName = intentHome.getStringExtra("cityName")
        var intentName = intentHome.getStringExtra("intentName")
        var intentIsRecent = intentHome.getBooleanExtra("intentRecent", false)


        weatherViewModel.onInitialExecution.observe(this, Observer {
            // to check from which screen we have landed here
            when (intentName) {
                "SPLASH" -> {
                    weatherViewModel.getIntentValues(homeActivityContext, intentCityName.toString(), false)
                }
                "FAVOURITE" -> {
                    weatherViewModel.getIntentValues(homeActivityContext, intentCityName.toString(), intentIsRecent)
                }
                "RECENT_SEARCH" -> {
                    weatherViewModel.getIntentValues(homeActivityContext, intentCityName.toString(), intentIsRecent)
                }
                else -> {
                    weatherViewModel.getIntentValues(homeActivityContext, "", false)
                }
            }

        })



        weatherViewModel.tempImageIconId.observe(this, Observer {id ->
            var imageUrl = "http://openweathermap.org/img/wn/$id@4x.png"
            Picasso.get().load(imageUrl).into(imgViewWeather)
        })
        weatherViewModel.radBtnCheckedCelsius.observe(this, Observer {
            if(it) {

                radioCelsius.setTextColor(ContextCompat.getColor(this, R.color.radioActiveColor))
                radioFahrenheit.setTextColor(ContextCompat.getColor(this,R.color.text_color))
                radioCelsius.text = "\u00B0C"

                radioCelsius.isChecked = true
            }
        })
        weatherViewModel.radBtnCheckedFahrenheit.observe(this, Observer {
            if(it){

                radioFahrenheit.text = "\u00B0F"
                radioFahrenheit.setTextColor(ContextCompat.getColor(this, R.color.radioActiveColor))
                radioCelsius.setTextColor(ContextCompat.getColor(this, R.color.text_color))
                radioFahrenheit.isChecked = true
            }
        })
        weatherViewModel.favLocation.observe(this, Observer {
            if(it) {
                tvBtnAddToFav.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_favourite_active,0,0,0)
                tvBtnAddToFav.text = "Added to favourite"
            } else {
                tvBtnAddToFav.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_favourite, 0,0,0)
                tvBtnAddToFav.text = "Add to favourite"
            }
        })

        homeNavigationView.setNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.homeMenu -> {
                    startActivity(Intent(this, HomeActivity::class.java))
                }
                R.id.recentSearchMenu -> {
                    startActivity(Intent(this, RecentSearchHistoryActivity::class.java))
                }
                R.id.favouritesMenu -> {
                    startActivity(Intent(this, FavouritesActivity::class.java))
                }

            }
            homeDrawerLayout.closeDrawer(GravityCompat.START)
            true
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_search_bar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.homeSearchBar -> {
                val intent = PlaceAutocomplete.IntentBuilder()
                    .accessToken("pk.eyJ1Ijoic2F2YW4ta3VtYXIiLCJhIjoiY2tleHNya3MxMDM4cTJ0b29wNnlwMzZmaSJ9.bG1PsrCH3_uVzbVNt7Y-Lg")
                    .placeOptions(PlaceOptions.builder().build(PlaceOptions.MODE_CARDS))
                    .build(this)

                startActivityForResult(intent,1)
            }
        }
        return true
       // return super.onOptionsItemSelected(item)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == 1) {
            var feature = PlaceAutocomplete.getPlace(data)
            searchValue =  feature.text()!!
           // Log.d("LogReport", "" + searchValue)

            weatherViewModel.getCurrentLocationApiData(this, searchValue, true)

        }
    }

    @SuppressLint("RestrictedApi")
    private fun setUpToolbar() {

        setSupportActionBar(this.customToolbar)
        supportActionBar?.setLogo(R.drawable.logo_home)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_menu_white)
        supportActionBar?.title = ""
        toggle = ActionBarDrawerToggle(this, homeDrawerLayout, this.customToolbar, R.string.open, R.string.close)
        homeDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

    }




}
