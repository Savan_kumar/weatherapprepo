package com.robosoft.myweatherapp.viewmodel
import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.JsonObject
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.api.WeatherApiClient
import com.robosoft.myweatherapp.model.ResponseDataClass
import com.robosoft.myweatherapp.model.WeatherData
import com.robosoft.myweatherapp.model.database.WeatherDataRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime.*
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.roundToInt

@RequiresApi(Build.VERSION_CODES.O)
class WeatherViewModel(val dataRepository: WeatherDataRepository): ViewModel(),Observable {

    val weatherData = dataRepository.weatherData
    lateinit var dataHolder: List<WeatherData>
    var tempKelvinData: Double = 0.0
    var tempKelvinMin: Double = 0.0
    var tempKelvinMax: Double = 0.0
    @Bindable
    var onInitialExecution = MutableLiveData<Boolean>()
    @Bindable
    val latitude = MutableLiveData<Double>()
    @Bindable
    val longitude = MutableLiveData<Double>()
    @Bindable
    val dateTime = MutableLiveData<String>()
    @Bindable
    val locationData = MutableLiveData<String>()
    @Bindable
    val cityID = MutableLiveData<Long>()
    @Bindable
    val cityName = MutableLiveData<String>()
    @Bindable
    val tempImageIconId = MutableLiveData<String>()
    @Bindable
    val temperatureValue = MutableLiveData<String>()
    @Bindable
    val temperatureValueKelvin = MutableLiveData<Double>()
    @Bindable
    val temperatureDescription = MutableLiveData<String>()
    @Bindable
    val minMaxTemperature = MutableLiveData<String>()
    @Bindable
    val precipitationRate = MutableLiveData<String>()
    @Bindable
    val humidity = MutableLiveData<String>()
    @Bindable
    val windSpeed = MutableLiveData<String>()
    @Bindable
    val weatherVisibility = MutableLiveData<String>()
    @Bindable
    val favLocation = MutableLiveData<Boolean>()
    @Bindable
    val isFavLocation = MutableLiveData<Boolean>()
    @Bindable
    val recentSearch = MutableLiveData<Boolean>()
    @Bindable
    val addToFavouriteButtonText = MutableLiveData<String>()
    @Bindable
    val radioBtnCelsius = MutableLiveData<String>()
    @Bindable
    val radioBtnFahrenheit = MutableLiveData<String>()
    @Bindable
    val radBtnCheckedCelsius = MutableLiveData<Boolean>()
    @Bindable
    val radBtnCheckedFahrenheit = MutableLiveData<Boolean>()

    lateinit var sharedPreferences: SharedPreferences

    var getRadioState: Boolean = false
    init{
        onInitialExecution.value = true
        radioBtnCelsius.value = "\u00B0 C"
        radioBtnFahrenheit.value = "\u00B0 F"
        precipitationRate.value = "0%"

        addToFavouriteButtonText.value = "Add to favourite"
        //set current date Time
        val curDateTime = now()
        dateTime.value = DateTimeFormatter.ofPattern(" EEE, dd MMM yyyy   hh:mm a").format(curDateTime)

    }

    //To know the previous class
    fun getIntentValues(context: Context, cityName: String, isRecentSearch: Boolean) {
        sharedPreferences = context.getSharedPreferences("MyWeatherPref", Context.MODE_PRIVATE)
        var myPrefEditor = sharedPreferences.edit()
        //get Previous Settings
        getRadioState = sharedPreferences.getBoolean("RadioStateCelsius", true)
        myPrefEditor.commit()
        when {
            cityName == "" -> {
               getLocationData(context)
            }
            cityName != "" -> {
                getCurrentLocationApiData(context, cityName, isRecentSearch )
            }
        }
    }

    fun addToFavourites() {

        if(favLocation.value == false) {
                favLocation.value = true
                insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!, temperatureValueKelvin.value!! ,temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!,  humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, favLocation.value!!, recentSearch.value!!))
                Log.d("MyTagInsert", "thisis inserted")
        } else {
            favLocation.value = false
            removeFavCity(cityID.value!!)
            Log.d("MyTagInsert", "thisis removed")
        }

    }
    private fun insertData(weatherData: WeatherData): Job = viewModelScope.launch {
            dataRepository.insertData(weatherData)
    }
    private fun removeFavCity(cityID: Long): Job = viewModelScope.launch {
        dataRepository.removeFav(cityID)
    }
    fun convertToFahrenheit() {

        var myPrefEditor = sharedPreferences.edit()
        //set button_state
        myPrefEditor.putBoolean("RadioStateCelsius", false)
        myPrefEditor.commit()
        radBtnCheckedFahrenheit.value = true
        temperatureValue.value = (((9.0/5.0) * (tempKelvinData  - 273.15) + 32).roundToInt()).toString()
        minMaxTemperature.value = ((((9.0/5.0) * (tempKelvinMin  - 273.15) + 32).roundToInt())).toString() + "\u00B0 - " + ((((9.0/5.0) * (tempKelvinMax  - 273.15) + 32).roundToInt()).toString()) + "\u00B0"

    }
    fun convertToCelsius() {
        var myPrefEditor = sharedPreferences.edit()
        //set button_state
        myPrefEditor.putBoolean("RadioStateCelsius", true)
        myPrefEditor.commit()
        radBtnCheckedCelsius.value = true
        temperatureValue.value = ((tempKelvinData - 273.15).roundToInt()).toString()
        minMaxTemperature.value = ((tempKelvinMin - 273.15).roundToInt()).toString() + "\u00B0 - " + ((tempKelvinMax - 273.15).roundToInt()).toString() + "\u00B0"
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

        Log.d("myTagInImplementation", "this is just implementation of removeOnPropertyChangedCallBack")
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

        Log.d("myTagInImplementation", "this is just implementation of addOnPropertyChangedCallBack")
    }



    ////
    private fun getLocationData(context: Context) {
        var returnValue = ""
        var fusedLocationProviderClient: FusedLocationProviderClient
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnSuccessListener{
                var location = it
                if(location == null){
                    Toast.makeText(context, "Some Problem in Fetching location", Toast.LENGTH_LONG).show()
                    Log.d("Error Tag", "Error in Location Access")
                }else {
                    var geocoder = Geocoder(context, Locale.getDefault())
                    var currentAddress = geocoder.getFromLocation(location.latitude, location.longitude, 1 )
                    locationData.value = currentAddress[0].locality + ",  " + currentAddress[0].adminArea
                    cityName.value = currentAddress[0].locality
                    latitude.value = location.latitude
                    longitude.value = location.longitude
                    returnValue = currentAddress[0].locality.toString()
                    getCurrentLocationApiData(context, returnValue, false)
                }
            }

        } else {
            //Toast.makeText(context, "Error in location", Toast.LENGTH_LONG).show()
            Log.d("MyTag", "inside else")
            requestPermissions(context as Activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 100)
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnSuccessListener{
                var location = it
                if(location == null){
                    Toast.makeText(context, "Some Problem in Fetching location", Toast.LENGTH_LONG).show()
                    Log.d("Error Tag", "Error in Location Access1")
                }else {
                    var geocoder = Geocoder(context, Locale.getDefault())
                    var currentAddress = geocoder.getFromLocation(location.latitude, location.longitude, 1 )
                    locationData.value = currentAddress[0].locality + ",  " + currentAddress[0].adminArea
                    cityName.value = currentAddress[0].locality
                    latitude.value = location.latitude
                    longitude.value = location.longitude
                    returnValue = currentAddress[0].locality.toString()
                    getCurrentLocationApiData(context, returnValue, false)
                }
            }
        }
    }
    fun initialTempConversion(tempKelvin: Double): String {
        return ((tempKelvin - 273.15).roundToInt()).toString()
    }


    fun getCurrentLocationApiData(context: Context, searchCityName: String, isRecentSearch: Boolean) {

        val weatherApiClient = WeatherApiClient(context)
        weatherApiClient.weatherApiInstance.getWeatherData(searchCityName, context.getString(R.string.weather_api_key)).enqueue(object : Callback<ResponseDataClass>{
            override fun onFailure(call: Call<ResponseDataClass>, t: Throwable) {
                Log.d("MyApiTag", t.message)
                Toast.makeText(context, "API CALL FAILED", Toast.LENGTH_LONG).show()

            }

            override fun onResponse(
                call: Call<ResponseDataClass>,
                response: Response<ResponseDataClass>
            ) {

                if(response.code() == 200) {
                    //on success
                    var responseBody = response.body()
                    cityID.value = responseBody!!.id
                    tempKelvinData = responseBody?.main?.get("temp").asDouble
                    tempKelvinMin = responseBody?.main?.get("temp_min").asDouble
                    tempKelvinMax = responseBody?.main?.get("temp_max").asDouble
                    temperatureValue.value = initialTempConversion(responseBody?.main?.get("temp").asDouble).toInt().toString()
                    temperatureValueKelvin.value = responseBody?.main?.get("temp").asDouble
                    humidity.value = responseBody?.main?.get("humidity").toString() + "%"
                    minMaxTemperature.value =  initialTempConversion( responseBody?.main?.get("temp_min").asDouble) +  "\u00B0 - " + initialTempConversion(responseBody?.main?.get("temp_max").asDouble) + "\u00B0"
                    weatherVisibility.value = responseBody!!.visibility
                    windSpeed.value = responseBody?.wind?.get("speed").toString()

                    var weather = responseBody?.weather?.get(0) as JsonObject
                    temperatureDescription.value =  weather.get("description").toString().replace("\"","").split(" ").joinToString(" ") { it.capitalize() }
                    tempImageIconId.value = weather.get("icon").toString().replace("\"", "")
                    latitude.value = responseBody?.coord?.get("lat").asDouble
                    longitude.value = responseBody?.coord?.get("lon").asDouble
                    var geocoder = Geocoder(context, Locale.getDefault())
                    var currentAddress = geocoder.getFromLocation(latitude.value!!, longitude.value!!, 1 )
                    locationData.value = currentAddress[0].locality + ",  " + currentAddress[0].adminArea
                    cityName.value = currentAddress[0].locality
                    if(getRadioState) {
                        convertToCelsius()

                    } else {
                        convertToFahrenheit()
                    }
                        if(isRecentSearch) {
                            recentSearch.value = true
                            dataHolder = dataRepository.getCityDetails(cityID.value!!)
                            if(dataHolder.isEmpty()) {
                                //if data is not present
                                isFavLocation.value = false
                                favLocation.value = false
                                insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!,temperatureValueKelvin.value!!, temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!, humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, isFavLocation.value!!, recentSearch.value!!))

                            } else {
                                var localData = dataHolder[0]
                                if(localData.favouriteCity) {
                                    isFavLocation.value = true
                                    favLocation.value = true
                                    insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!, temperatureValueKelvin.value!!, temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!, humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, isFavLocation.value!!, recentSearch.value!!))

                                } else {
                                    isFavLocation.value = false
                                    favLocation.value = false
                                    insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!, temperatureValueKelvin.value!!, temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!, humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, isFavLocation.value!!, recentSearch.value!!))
                                }
                            }

                        } else {
                            recentSearch.value = false
                            dataHolder = dataRepository.getCityDetails(cityID.value!!)
                            if(dataHolder.isEmpty()) {
                                //if data is not present
                                favLocation.value = false
                                isFavLocation.value = false
                                insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!, temperatureValueKelvin.value!!, temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!, humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, isFavLocation.value!!, recentSearch.value!!))

                            } else {
                                var localData = dataHolder[0]
                                if(localData.favouriteCity) {
                                    isFavLocation.value = true
                                    favLocation.value = true
                                    insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!, temperatureValueKelvin.value!!, temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!, humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, isFavLocation.value!!, recentSearch.value!!))

                                } else {
                                    isFavLocation.value = false
                                    favLocation.value = false
                                    insertData(WeatherData(cityID.value!!,latitude.value!!, longitude.value!!, dateTime.value!!, locationData.value!!, cityName.value!!, tempImageIconId.value!!, temperatureValue.value!!, temperatureValueKelvin.value!!, temperatureDescription.value!!, minMaxTemperature.value!!, precipitationRate.value!!, humidity.value!!, windSpeed.value!!, weatherVisibility.value!!, isFavLocation.value!!, recentSearch.value!!))
                                }
                            }
                        }

                } else {
                    //on error
                    Log.d("MyAPiTAg", "Error wd api response")
                    Toast.makeText(context, "Error in API Response", Toast.LENGTH_LONG).show()


                }
            }

        })
    } // end of api call



}