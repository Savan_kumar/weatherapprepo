/*
 * *****************************************************************************
 * RecentSearchBlankActivity.kt
 * MyWeatherApp
 *
 * Created by savankumar on 15/9/20 12:01 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.myweatherapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.robosoft.myweatherapp.R

class RecentSearchBlankActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recent_search_blank)

    }
}
