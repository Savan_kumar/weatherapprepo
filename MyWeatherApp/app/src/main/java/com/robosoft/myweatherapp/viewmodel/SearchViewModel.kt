/*
 * *****************************************************************************
 * SearchViewModel.kt
 * MyWeatherApp
 *
 * Created by savankumar on 22/9/20 3:00 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.myweatherapp.viewmodel

import android.content.DialogInterface
import android.util.Log
import android.view.View
import androidx.databinding.Observable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.model.WeatherData
import com.robosoft.myweatherapp.model.database.WeatherDataRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class SearchViewModel (val dataRepository: WeatherDataRepository): ViewModel(), Observable {

    val recentSearchWeatherData = dataRepository.recentWeatherData


    fun removeOrAddToFavourites(weatherData: WeatherData) {


        if(!weatherData.favouriteCity) {
            //update favourite city
            insertData(WeatherData(weatherData.locationCityId, weatherData.cordLat, weatherData.cordLon, weatherData.dataTimeValue, weatherData.locationDataValue, weatherData.cityName, weatherData.imageIconId, weatherData.temperatureValue, weatherData.temperatureValueKelvin, weatherData.temperatureDescriptionValue, weatherData.minMaxValue,  weatherData.precipitationValue, weatherData.humidityValue, weatherData.windValue, weatherData.visibilityValue, true, weatherData.recentSearch))

        } else {
            removeFavCity(weatherData.locationCityId)
        }
    }

    private fun insertData(weatherData: WeatherData): Job = viewModelScope.launch {
        dataRepository.insertData(weatherData)
    }
    private fun removeFavCity(cityID: Long): Job = viewModelScope.launch {
        dataRepository.removeFav(cityID)
    }

    fun clearAllSearchData(view: View) {
        var context = view.context
        val alertDialog = androidx.appcompat.app.AlertDialog.Builder(context, R.style.CustomDialog)
        alertDialog.setMessage("Are you sure you want to clear your recent search history?")
        alertDialog.setPositiveButton("YES") {dialog: DialogInterface?, which: Int ->
            clearSearchData()
        }
        alertDialog.setNegativeButton("NO") {dialog: DialogInterface?, which: Int ->
            //
        }

        alertDialog.create()
        alertDialog.show()
    }

    private fun clearSearchData(): Job = viewModelScope.launch {
        dataRepository.removeAllSearchData()
    }


    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

}