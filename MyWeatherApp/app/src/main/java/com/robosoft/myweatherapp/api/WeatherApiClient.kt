/*
 * *****************************************************************************
 * WeatherApiClient.kt
 * MyWeatherApp
 *
 * Created by savankumar on 15/9/20 2:55 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.myweatherapp.api

import android.content.Context
import android.util.Log
import com.robosoft.myweatherapp.helperclass.isNetAvailable
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WeatherApiClient(context: Context) {
    private val BASE_URL = "http://api.openweathermap.org/data/2.5/"
    val cacheSize = (5 * 1024 * 1024).toLong()
    val myCache = Cache(context.cacheDir, cacheSize)

    private val okHttpClient = OkHttpClient.Builder()
        .cache(myCache)
        .addInterceptor { chain ->
            var request = chain.request()

            //check network code remaining
            Log.d("MyTagOfNetwor", isNetAvailable(context)!!.toString())
            request = if(isNetAvailable(context)!!)
                request.newBuilder().header("Cache-Control", "public, max-age=" + 60 * 30).build()
            else
                request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 30).build()
            chain.proceed(request)
        }
        .build()
    val weatherApiInstance: WeatherApiInterface by lazy{
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
        retrofit.create(WeatherApiInterface::class.java)
    }

}