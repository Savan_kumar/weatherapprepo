package com.robosoft.myweatherapp.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.robosoft.myweatherapp.model.WeatherData

@Database(entities = [WeatherData::class], version = 1)
abstract class WeatherDataBase: RoomDatabase() {

    abstract val weatherDao: WeatherDAO

    companion object {
        @Volatile
        private var INSTANCE: WeatherDataBase? = null
            fun getInstance(context: Context): WeatherDataBase {
                synchronized(this) {
                    var instance: WeatherDataBase? = INSTANCE
                    if(instance==null) {
                        instance = Room.databaseBuilder(
                            context.applicationContext,
                            WeatherDataBase::class.java,
                            "weather_data_database"
                        ).allowMainThreadQueries()
                            .build()
                    }
                    return instance
                }
            }
    }
}