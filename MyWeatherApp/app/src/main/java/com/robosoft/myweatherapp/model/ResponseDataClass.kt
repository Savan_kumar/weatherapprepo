package com.robosoft.myweatherapp.model

import com.google.gson.JsonArray
import com.google.gson.JsonObject



class ResponseDataClass(
    val coord: JsonObject,
    val weather: JsonArray,
    val main: JsonObject,
    val visibility: String,
    val wind: JsonObject,
    val id: Long,
    val sys: JsonObject
) {


}