package com.robosoft.myweatherapp.model.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.robosoft.myweatherapp.model.WeatherData

@Dao
interface WeatherDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeatherData(weatherData: WeatherData)

    @Query("DELETE FROM weather_data")  // deletes all data from table
    suspend fun deleteAll()

    @Query("SELECT * FROM weather_data")  //gets all weather data
    fun getAllWeatherData(): LiveData<List<WeatherData>>

    @Query("SELECT * FROM weather_data WHERE favouriteCity = :boolValue") //gets all fav city
    fun getAllFavourites(boolValue: Boolean): LiveData<List<WeatherData>>

    @Query("SELECT * FROM weather_data WHERE recentSearch = :boolValue")   //gets all recent search
    fun getAllRecentSearch(boolValue: Boolean): LiveData<List<WeatherData>>



    @Query("UPDATE weather_data SET favouriteCity =:boolValue WHERE locationCityId = :cityID")   // removes fav city
    suspend fun removeCityFromFavourites(cityID: Long, boolValue: Boolean)

    @Query("SELECT * FROM weather_data WHERE locationCityId = :cityID")
    fun getCityDetailByID(cityID: Long): List<WeatherData>

    @Query("UPDATE weather_data SET favouriteCity =:boolValue WHERE favouriteCity = :boolValueTrue")
    suspend fun removeAllFavCity(boolValue: Boolean, boolValueTrue: Boolean)

    @Query("UPDATE weather_data SET recentSearch =:boolValue WHERE recentSearch = :boolValueTrue")
    suspend fun removeAllRecentData(boolValue: Boolean, boolValueTrue: Boolean)
}