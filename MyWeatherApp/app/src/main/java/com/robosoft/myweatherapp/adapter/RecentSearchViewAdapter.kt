/*
 * *****************************************************************************
 * RecentSearchViewAdapter.kt
 * MyWeatherApp
 *
 * Created by savankumar on 22/9/20 3:41 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.myweatherapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.databinding.FavouritesCardviewBinding
import com.robosoft.myweatherapp.helperclass.tempConversionToCelsius
import com.robosoft.myweatherapp.model.WeatherData
import com.squareup.picasso.Picasso

class RecentSearchViewAdapter(val weatherDataList: List<WeatherData>,
                              var clickListener: (WeatherData) -> Unit,
                              var btnClickListener: (WeatherData) -> Unit,
                              var context: Context
                              ): RecyclerView.Adapter<RecentSearchViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentSearchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: FavouritesCardviewBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.favourites_cardview, parent, false)

        return RecentSearchViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return weatherDataList.size
    }

    override fun onBindViewHolder(holder: RecentSearchViewHolder, position: Int) {
        holder.bindData(weatherDataList[position], clickListener, btnClickListener, context)
    }

}


class RecentSearchViewHolder(val binding: FavouritesCardviewBinding): RecyclerView.ViewHolder(binding.root) {
  fun bindData(weatherData: WeatherData,
               clickListener: (WeatherData) -> Unit,
               btnClickListener: (WeatherData) -> Unit,
               context: Context) {

      var sharedPreferences = context.getSharedPreferences("MyWeatherPref", Context.MODE_PRIVATE)

      //get Previous Settings
      var getRadioState = sharedPreferences.getBoolean("RadioStateCelsius", true)
      if(getRadioState) {
          binding.tvTempDegree.text = tempConversionToCelsius(weatherData.temperatureValueKelvin.toDouble(), true).toString()
          binding.celsiusText.text = "\u00B0C"
      } else {
          binding.tvTempDegree.text = tempConversionToCelsius(weatherData.temperatureValueKelvin.toDouble(), false).toString()
          binding.celsiusText.text = "\u00B0F"
      }
      binding.tvLocationCardView.text = weatherData.locationDataValue

      binding.tvTempDescriptionCardView.text = weatherData.temperatureDescriptionValue
      if(weatherData.favouriteCity) {
          binding.btnFavouritesCardView.setImageResource(R.drawable.icon_favourite_active)
      } else {
          binding.btnFavouritesCardView.setImageResource(R.drawable.icon_favourite)
      }
      (Picasso.get().load("http://openweathermap.org/img/wn/${weatherData.imageIconId}@2x.png").into(binding.ivTempTypeCardView))

      binding.favouritesCardView.setOnClickListener {
          clickListener(weatherData)
      }
      binding.btnFavouritesCardView.setOnClickListener {
          btnClickListener(weatherData)
      }
  }
}

