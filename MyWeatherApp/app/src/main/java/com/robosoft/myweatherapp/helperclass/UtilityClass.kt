/*
 * *****************************************************************************
 * UtilityClass.kt
 * MyWeatherApp
 *
 * Created by savankumar on 16/9/20 8:47 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.myweatherapp.helperclass

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import kotlin.math.roundToInt

class UtilityClass {
}

fun isNetAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetwork
    var isConnected: Boolean = false
    if(activeNetwork != null) {
        Log.d("MyUtilityTag", "InsideSsNetAvailable")
        val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
        if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
            || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH)) {
            isConnected = true
        }
    }
    return isConnected
}
fun tempConversionToCelsius(tempK:Double, toCelsius: Boolean): Int {
    return if(toCelsius) {
        ((tempK - 273.15).roundToInt())
    } else {
        ((((9.0/5.0) * (tempK  - 273.15) + 32).roundToInt()))
    }
}

