package com.robosoft.myweatherapp.model.database

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.robosoft.myweatherapp.viewmodel.FavouritesViewModel
import com.robosoft.myweatherapp.viewmodel.SearchViewModel
import com.robosoft.myweatherapp.viewmodel.WeatherViewModel
import java.lang.IllegalArgumentException

class WeatherViewModelFactory(private val weatherDataRepository: WeatherDataRepository): ViewModelProvider.Factory{
    @RequiresApi(Build.VERSION_CODES.O)
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(WeatherViewModel::class.java)){
            return WeatherViewModel(weatherDataRepository) as T

        }
        else if(modelClass.isAssignableFrom(FavouritesViewModel::class.java)) {
            return FavouritesViewModel(weatherDataRepository) as T
        }
        else if(modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(weatherDataRepository) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }

}