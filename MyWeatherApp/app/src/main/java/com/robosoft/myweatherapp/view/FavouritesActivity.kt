package com.robosoft.myweatherapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.robosoft.myweatherapp.R
import com.robosoft.myweatherapp.adapter.FavouritesRecyclerViewAdapter
import com.robosoft.myweatherapp.databinding.ActivityFavouritesBinding
import com.robosoft.myweatherapp.model.WeatherData
import com.robosoft.myweatherapp.model.database.WeatherDAO
import com.robosoft.myweatherapp.model.database.WeatherDataBase
import com.robosoft.myweatherapp.model.database.WeatherDataRepository
import com.robosoft.myweatherapp.model.database.WeatherViewModelFactory
import com.robosoft.myweatherapp.viewmodel.FavouritesViewModel


class FavouritesActivity : AppCompatActivity() {

    lateinit var binding: ActivityFavouritesBinding
    lateinit var favViewModel: FavouritesViewModel
    lateinit var adapter : FavouritesRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favourites)
        val dao: WeatherDAO = WeatherDataBase.getInstance(application).weatherDao

        val weatherRepository = WeatherDataRepository(dao)
        val factory = WeatherViewModelFactory(weatherRepository)
        favViewModel = ViewModelProvider(this, factory).get(FavouritesViewModel::class.java)
        binding.myFavouritesViewModel = favViewModel
        binding.lifecycleOwner = this

        initRecyclerView() // updates recyclerview data from database


    }
    private fun listItemClicked(weatherData: WeatherData) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra("intentName", "FAVOURITE")
        intent.putExtra("cityName", weatherData.cityName)
        intent.putExtra("intentRecent", weatherData.recentSearch)
        startActivity(intent)

    }

    private fun listItemRemoved(weatherData: WeatherData ) {
        favViewModel.removeFavouriteCity(weatherData.locationCityId)
    }

    private fun initRecyclerView() {
        binding.favouritesRecyclerView.layoutManager = LinearLayoutManager(this)

        displayAllFavCities()
    }
    private fun displayAllFavCities() {
        favViewModel.favWeatherData.observe(this, Observer {

            when {
                it.isEmpty() -> {
                    startActivity(Intent(this, FavouritesBlankActivity::class.java))
                }
                it.size == 1 -> {
                    binding.tvNumberOfCities.text = it.size.toString() + " City added as favourite"
                }
                else -> {
                    binding.tvNumberOfCities.text = it.size.toString() + " Cities added as favourites"
                }
            }
             binding.favouritesRecyclerView.adapter = FavouritesRecyclerViewAdapter(it, { selectedItem: WeatherData -> listItemClicked(selectedItem)} , {removeItem: WeatherData -> listItemRemoved(removeItem)}, this)

        })
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.fav_search_bar_menu, menu)
        val searchItem = menu?.findItem(R.id.favouritesSearchView)

        var searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false

            }
            override fun onQueryTextChange(newText: String?): Boolean {

                return false
            }
        })

        return true
    }

}


