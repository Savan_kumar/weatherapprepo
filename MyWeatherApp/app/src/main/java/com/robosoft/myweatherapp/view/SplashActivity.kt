package com.robosoft.myweatherapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    private val handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_splash)

    }
    private val runnable = Runnable {
        if(!isFinishing) {
           // Toast.makeText(this,"lol", Toast.LENGTH_LONG).show()
            var intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("intentName", "SPLASH")
            intent.putExtra("cityName", "")
            intent.putExtra("intentRecent", false)
            startActivity(intent)
            finish()
        }
    }
    override fun onResume() {
        super.onResume()
        handler.postDelayed(runnable, 3000)
    }
    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

}
